
function apply() {
    var L1 = document.getElementById("Ligue1").checked;
    var Liga = document.getElementById("Liga").checked;
    var PL = document.getElementById("PL").checked;

    if(L1){
        
        var elems = document.getElementsByClassName('L1');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'block';
        }
    }
    else {
        var elems = document.getElementsByClassName('L1');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'none';
        }
        
    }

    if(Liga){
        
        var elems = document.getElementsByClassName('Liga');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'block';
        }
    }
    else {
        var elems = document.getElementsByClassName('Liga');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'none';
        }
        
    }

    if(PL){
        
        var elems = document.getElementsByClassName('PL');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'block';
        }
    }
    else {
        var elems = document.getElementsByClassName('PL');
        for (var i=0;i<elems.length;i+=1){
            elems[i].style.display = 'none';
        }
        
    }
}

function afficher(){
    var suscribeModal = document.getElementById('suscribeModal');
    if (typeof suscribeModal.showModal === "function") {
        suscribeModal.showModal();
    } else {
    console.error("L'API <dialog> n'est pas prise en charge par ce navigateur.");
    }
}

function close1(){
    var suscribeModal = document.getElementById('suscribeModal');
    suscribeModal.close();
}

function connect(){
    var connectModal = document.getElementById('connectModal');
    if (typeof connectModal.showModal === "function") {
        connectModal.showModal();
    } else {
    console.error("L'API <dialog> n'est pas prise en charge par ce navigateur.");
    }
}

function close2(){
    var connectModal = document.getElementById('connectModal');
    connectModal.close();
}

var session = new Array()

function registration() {

    firstName = document.getElementById("firstName").value;
    lastName = document.getElementById("lastName").value;
    email= document.getElementById("email").value;
    tel = document.getElementById("tel").value;
    pseudo = document.getElementById("pseudo").value;
    password = document.getElementById("password").value;
    const storage = new Array(firstName, lastName, email, tel, pseudo, password);
    if (localStorage.getItem(pseudo)){
        alert("You are already registered, please log in");
        close1();
        connect();
    }

    else{
        localStorage.setItem(pseudo, JSON.stringify(storage));
        session[0] = pseudo
        session[1] = password
        sessionStorage.setItem(pseudo, JSON.stringify(session));
        close1();
        document.getElementById("suscribeBtn").style.visibility= "hidden";
        document.getElementById("connexionBtn").style.visibility= "hidden";
        document.getElementById("deconnexionBtn").style.visibility= "visible";
        displayComments();
    }

}


function login() {

    pseudo = document.getElementById("pseudo2").value;
    password = document.getElementById("password2").value;
    storage = JSON.parse(localStorage.getItem(pseudo))
    console.log(storage[5])
    if(storage[5] == password){
        session[0] = pseudo
        session[1] = password
        sessionStorage.setItem(pseudo, JSON.stringify(session));
        close2();
        document.getElementById("suscribeBtn").style.visibility= "hidden";
        document.getElementById("connexionBtn").style.visibility= "hidden";
        document.getElementById("deconnexionBtn").style.visibility= "visible";
        displayComments();
    }

    else{
        alert("Wrong password !")
    }

}

function logOut(){
    sessionStorage.removeItem(session[0])
    document.getElementById("suscribeBtn").style.visibility= "visible";
    document.getElementById("connexionBtn").style.visibility= "visible";
    document.getElementById("deconnexionBtn").style.visibility= "hidden";
}

function dropHandler(ev) {
    console.log('File(s) dropped');
  
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
  
    if (ev.dataTransfer.items) {
      // Use DataTransferItemList interface to access the file(s)
      for (var i = 0; i < ev.dataTransfer.items.length; i++) {
        // If dropped items aren't files, reject them
        if (ev.dataTransfer.items[i].kind === 'file') {
          var file = ev.dataTransfer.items[i].getAsFile();
          console.log('... file[' + i + '].name = ' + file.name);
          console.log(file)
          document.getElementById("imageprofile").appendChild()
                }
      }
    } else {
      // Use DataTransfer interface to access the file(s)
      for (var i = 0; i < ev.dataTransfer.files.length; i++) {
        console.log('... file[' + i + '].name = ' + ev.dataTransfer.files[i].name);
        console.log(ev.dataTransfer.files[i])
      }
    }
}

function dragOverHandler(ev) {
    console.log('File(s) in drop zone');
    
    // Prevent default behavior (Prevent file from being opened)
    ev.preventDefault();
}



function addComment(){
    if(session[0] != undefined){
        console.log("1")
        const commentary = document.getElementById("comment").value;
        today = new Date();

        let date = today.getDate();
        let month = today.getMonth()+1;
        let year = today.getYear()+1900;

        
        let newcomment = '<p>«'+commentary+'»'+' '+ session[0] + ', '+date.toString()+'/'+month.toString()+'/'+year.toString()+'</p>';
        let lengthlocalstorage = localStorage.length;
        localStorage.setItem("comment"+lengthlocalstorage, newcomment);
        displayComments();
    }
    else{
        console.log("2")
        alert("You are not logged in")
    }
}



function displayComments(){
    let lengthlocalstorage = localStorage.length;
    let str = '';
    for (i=0; i<lengthlocalstorage; i++){
        let currentkey = localStorage.key(i);
        const regex = /^comment/;
        if (currentkey.match(regex)){
            console.log(currentkey)
            let currentcomment = localStorage.getItem(currentkey);
            str = str + currentcomment +'\n';
        }
    }
    console.log(str)
    document.getElementById("displaycomments").innerHTML = str;
}
